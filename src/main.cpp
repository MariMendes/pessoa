#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;

int main(int argc, char ** argv) {

	Pessoa aluno_1;
	Pessoa * aluno_2;
	Pessoa * aluno_3;
	Aluno * aluno_4;
	Professor *professor_1;

	aluno_1.setNome("Bruno");
	aluno_1.setTelefone("555-4444");
	aluno_1.setIdade("14");

	aluno_2 = new Pessoa(); 

	aluno_2->setNome("Maria");
	aluno_2->setTelefone("333-5555");
	aluno_2->setIdade("54");

	aluno_3 = new Pessoa("Joao","35","222-5555");

	aluno_4 = new Aluno();
	aluno_4->setNome("Mariana");
	aluno_4->setIdade("19");
	aluno_4->setTelefone("555-0000");
	aluno_4->setMatricula(101020);
	aluno_4->setQuantidadeCreditos(32);
	aluno_4->setSemestre(3);
	aluno_4->setIra(3.0);

	professor_1 = new Professor();
	professor_1->setNome("Renato");
	professor_1->setIdade("30");
	professor_1->setTelefone("555-0001");
	professor_1->setDisciplina("Orientacao a Objetos");
	professor_1->setMatricula_F(101021);
	professor_1->setCarga_horaria(40);
	professor_1->setodigo_da_materia(120150)
	

	cout << "Nome\tIdade\tTelefone" << endl;
	cout << aluno_1.getNome() << "\t" << aluno_1.getIdade() << "\t" << aluno_1.getTelefone() << endl;
	cout << aluno_2->getNome() << "\t" << aluno_2->getIdade() << "\t" << aluno_2->getTelefone() << endl;
	cout << aluno_3->getNome() << "\t" << aluno_3->getIdade() << "\t" << aluno_3->getTelefone() << endl;

	cout << "Nome\tIdade\tTelefone\tMatricula\tQuantidadeCreditos\tSemestre\tIra" << endl;
	cout << aluno_4.getNome() << endl;
	cout << aluno_4.getIdade() << endl;
	cout << aluno_4.getTelefone() << endl;
	cout << aluno_4.getMatricula() << endl;
	cout << aluno_4.getQuantidadeCreditos() << endl;
	cout << aluno_4.getSemestre() << endl;
	cout << aluno_4.getIra() << endl;

	cout <<"Nome\tIdade\tTelefone\tDisciplina\tMatricula\tCarga_horaria\tCodigo_da_materia" << endl;
	cout << professor_1.getNome() << endl;
	cout << professor_1.getIdade() << endl;
	cout << professor_1.getTelefone() << endl;
	cout << professor_1.getDisciplina() << endl;
	cout << professor_1.getMatricula() << endl:
	cout << professor_1.getCarga_horaria() << endl;
	cout << professor_1.getCodigo_da_materia() << endl;

	delete(aluno_2);
	delete(aluno_3);
	delete(aluno_4);
	delete(professor_1);

//return 0;
}



