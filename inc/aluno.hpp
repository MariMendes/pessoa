#ifndef ALUNO_H
#define ALUNO_H

#include "pessoa.hpp" 

class Aluno: public pessoa {
		private:
			int matricula;
			int quantidade_de_creditos;
			int semestre;
			float ira;
         
        public:
        	Aluno();
        	Aluno(string nome, string idade, string telefone, int matricula);
        	void setMatricula(int matricula);
        	int getMatricula();
        	void setQuantidadeCreditos(int credidos);
        	int getQuantidadeCreditos();
        	void setSemestre(int semestre);
        	int getSemestre();
        	string getSemestreString();
        	void setIra(float ira);
        	float setIra();

};


#endif
