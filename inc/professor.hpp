#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

//using namespace std;

class Professor: public pessoa {
private:
	string disciplina;
	int matricula;
	int carga_horaria;
	int codigo_da_materia;

public:
	Professor();
	Professor(string nome, string idade, string telefone, string disciplina, int matricula, int carga_horaria, int codigo_da_materia);
	string getDisciplina();
	void setDisciplina(string disciplina);
	int getMatricula();
	void setMatricula(int matricula);
	int getCarga_horaria();
	void setCarga_horaria(int carga);
	int getCodigo_da_materia();
	void setCodigo_da_materia(int codigo);
};	
 #endif